alter table place_best_time modify column place_id int NULL;
ALTER TABLE place_best_time  ADD spot_id INT NULL;
ALTER TABLE place_best_time ADD CONSTRAINT fk_spot_id FOREIGN KEY (spot_id) REFERENCES spot(id);


create table place_type(
  id int NOT NULL AUTO_INCREMENT,
  type varchar(50) NOT NULL,
  status tinyint(1) not null default 1,
  PRIMARY KEY (id)
);

INSERT INTO place_type(type) values('place');
INSERT INTO place_type(type) values('spot');

ALTER TABLE place_best_time  ADD place_type_id INT NULL;
ALTER TABLE place_best_time modify column  place_type_id INT NOT NULL;
update place_best_time set place_type_id=1;
ALTER TABLE place_best_time ADD CONSTRAINT fk_place_type_id FOREIGN KEY (place_type_id) REFERENCES place_type(id);

create table best_time_submit(
  id int NOT NULL AUTO_INCREMENT,
  place_best_time_id int NOT NULL,
  detail text not null,
  insta varchar(50) default null,
  created_on datetime not null,
  PRIMARY KEY (id),
  FOREIGN KEY (place_best_time_id) REFERENCES place_best_time(id)
);

/* Get place details by place_best_time_id including 1 image */
SELECT pbt.*, p.name, i.url as image from place_best_time as pbt, 
place as p, image as i, place_image as pi where pbt.place_id = p.id 
and pbt.id = 4 and pi.image_id=i.id and pi.place_id=p.id limit 1;


select pbt.id, p.name as place_name, s.name as spot_name 
from place_best_time as pbt, spot as s, place as p 
where pbt.place_id = p.id or pbt.spot_id=s.id;

alter table place_best_time add column last_hit_on datetime not null;