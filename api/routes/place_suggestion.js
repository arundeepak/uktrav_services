const express = require('express');
const router = express.Router();
const util = require('util');
const moment = require('moment-timezone');
//const db = require('./../models/index');
const db = require('./../models/db');

// Get All places mentioned in Place Best Time table
/*
router.get('/', (req, res, next) => {
  stmt = 'select p.name, pbt.*, pt.type from place as p, place_best_time as pbt, place_type as pt where pbt.place_id = p.id and pbt.place_type_id = pt.id';
  db.query(stmt, (error, places , _) => {
    var res_error = ''
    var result = true
    if(places === undefined){
      res_error = 'No Such Data';
      result = false;
    }
    if(result){
        res.status(200).json({
              result: result,
              error: res_error,
              places: places
        });
    }else{
      res.status(200).json({
            "result": result,
            "error": res_error
      });
    }
  });
});

*/

router.get('/', (req, res, next) => {
  stmt1 = 'select p.name, pbt.id from place as p, place_best_time as pbt where pbt.place_type_id = 1 and pbt.place_id=p.id';
  stmt2 = 'select s.name, pbt.id from spot as s, place_best_time as pbt where pbt.place_type_id = 2 and pbt.spot_id=s.id';
  db.query(stmt1+';'+stmt2, (error, results , _) => {
    var res_error = '';
    var result = true;
    var places = [];
    var placeMap = {};
    if(error){
        res.status(200).json({
              result: result,
              error: error
        });
    }else{
      results[0].forEach(place =>{
        places.push(place.name);
        placeMap[place.name] = place.id;
      });

      results[1].forEach(place =>{
        places.push(place.name);
        placeMap[place.name] = place.id;
      });

      //recent searched places 
      recentSearchStmt1 = util.format('select * from place_best_time where place_type_id =1 order by last_hit_on desc limit 1');
      recentSearchStmt2 = util.format('select * from place_best_time where place_type_id =2 order by last_hit_on desc limit 1');
      
      db.query(recentSearchStmt1+';'+recentSearchStmt2, (error, searchResults , _) => {
        recentSearches = searchResults[0];
        recentSearches1 = searchResults[1];
        Array.prototype.push.apply(recentSearches,recentSearches1); 
        res.status(200).json({
            result: result,
            error: res_error,
            places: places,
            placeMap: placeMap,
            recentSearches: recentSearches
        });
    });
    }
  });
});


// Get Place name and ID
router.get('/:pbtId', (req, res, next) => {
  var datetime = new Date();
  var datetime = moment(datetime).tz("Asia/Kolkata");
  datetime = datetime.format('YYYY-MM-DD HH:mm:ss');
  updateStmt = util.format('update place_best_time set last_hit_on = "%s" where id=%s',(datetime),(req.params.pbtId));
  db.query(updateStmt, (error, place_best_time, _) => {});
  stmt = util.format('select * from place_best_time where id = %s ;' ,(req.params.pbtId));
  db.query(stmt, (error, place_best_time, _) => {
      var res_error = ''
      var result = true
      if(place_best_time === undefined || place_best_time.length == 0){
        res_error = 'No Such Data';
        result = false;
        res.status(200).json({
          "result": result,
          "error": res_error
        });
      }

      if(place_best_time[0].place_type_id === 1){
        stmt = util.format('SELECT pbt.*, p.name, i.url as image from place_best_time as pbt, place as p, image as i, place_image as pi where pbt.place_id = p.id and pbt.id = %s and pi.image_id=i.id and pi.place_id=p.id limit 1;' ,(req.params.pbtId));
        db.query(stmt, (error, place, _) => {
          if(place === undefined || place.length == 0){
            res_error = 'No Such Data';
            result = false;
            res.status(200).json({
              "result": result,
              "error": res_error
            });
          }
          place = place[0]
          res.status(200).json({
            result: result,
            error: res_error,
            place: place
          });
        });      
      }else if(place_best_time[0].place_type_id === 2){
        stmt = util.format('SELECT pbt.*, s.name, i.url as image from place_best_time as pbt, spot as s, image as i, spot_image as si where pbt.spot_id = s.id and pbt.id = %s and si.image_id=i.id and si.spot_id=s.id limit 1;' ,(req.params.pbtId));
        db.query(stmt, (error, spot, _) => {
          if(spot === undefined || spot.length == 0){
            res_error = 'No Such Data';
            result = false;
            res.status(200).json({
              "result": result,
              "error": res_error
            });
          }
          spot = spot[0]
          res.status(200).json({
            result: result,
            error: res_error,
            place: spot
          });
        });      
      }
  	});
});


router.post('/info/:pbtId', (req, res, next) => {
    result = true
    res_error = ''
    detail = req.body.detail;
    username = req.body.username;
    if(detail === undefined){
      result = false
      res_error = 'Best Time Detail is missing in the request!'
    }else{
      if(username == undefined){
        username = null;
      }
      var datetime = new Date();
      var datetime = moment(datetime).tz("Asia/Kolkata");
      datetime = datetime.format('YYYY-MM-DD HH:mm:ss');
      var stmt = util.format('INSERT INTO best_time_submit(place_best_time_id, detail, insta, created_on) VALUES(%s,"%s","%s","%s");',(req.params.pbtId),(detail),(username),(datetime));
      console.log('stmt', stmt);
      db.query(stmt, function (err, result) {
        if(err){
          result = false;
          res_error = err;
        }else{
          console.log("1 record inserted in best_time_submit for " + req.params.pbtId);
          email_list = 'bhartendumehta206@gmail.com,arundeepak92@gmail.com';
          subject = 'New Edit request for Place Best Time id '+ req.params.pbtId;
          body = detail;
          email_stmt = util.format('INSERT INTO email_job(recipients, subject, body, sent) VALUES("%s","%s","%s",%s);',(email_list),(subject),(body),(0));
          db.query(email_stmt, function (err, result) {
            if(err){
              result = false;
              res_error = err;
            }else{
              console.log('Email sent');
            }
          });
        }
      });
    }
    res.status(200).json({
          "result": result,
          "error": res_error
    });
});


module.exports = router;
