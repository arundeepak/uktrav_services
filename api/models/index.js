const Sequelize = require('sequelize');

const sequelize = new Sequelize('uktrav', 'uktraveller', 'uttarakhand', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  operatorsAliases: false,
  define: {
      freezeTableName: true,
      timestamps: false
  }
});


const Place = sequelize.define('place', {
  id: { type: Sequelize.INTEGER, primaryKey: true },
  districtId:{ type: Sequelize.INTEGER, field: 'district_id'},
  name: Sequelize.STRING,
  cr_date: Sequelize.DATE,
  status: Sequelize.INTEGER,
});

const District = sequelize.define('district', {
  id: { type: Sequelize.INTEGER, primaryKey: true },
  name: Sequelize.STRING,
  division: Sequelize.STRING
});

const PlaceBestTime = sequelize.define('place_best_time', {
  id: { type: Sequelize.INTEGER, primaryKey: true },
  place_id: { type: Sequelize.INTEGER},
  detail: Sequelize.STRING
});


// Relationships
PlaceBestTime.belongsTo(Place);
Place.belongsTo(District);


module.exports = {
  Place: Place,
  District : District,
  PlaceBestTime: PlaceBestTime
};
